FROM maven:3.6.0-jdk-8 AS build  
COPY . .
RUN pwd
RUN mvn clean package 
RUN ls ./target


FROM openjdk:9  
COPY --from=build /target/simple-maven-app-1.0.jar /usr/app/simple-maven-app-1.0.jar  
EXPOSE 8091  
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=test","/usr/app/simple-maven-app-1.0.jar"] 

